﻿using System;
using SQLite.Net;
using Xamarin.Forms;
using System.IO;
using MovieApp.Droid;

[assembly: Dependency (typeof (SQLite_Android))]
namespace MovieApp.Droid
{
    public class SQLite_Android : ISQLite
    {
        public SQLite_Android()
        {
        }

        public SQLiteConnection GetConnection () 
        {
            var sqliteFilename = "MoviesSQLite.db3";

            // Documents folder
            string documentsPath = System.Environment.GetFolderPath (System.Environment.SpecialFolder.Personal); 
            var path = Path.Combine(documentsPath, sqliteFilename);
            // Get Xamarin Android platform
            var platform = new SQLite.Net.Platform.XamarinAndroid.SQLitePlatformAndroid ();
            // Create the connection
            var conn = new SQLiteConnection(platform, path);
            // Return the database connection
            return conn;
        }
    }
}


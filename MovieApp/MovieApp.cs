﻿using System;

using Xamarin.Forms;
using System.Reflection;
using System.IO;
using Newtonsoft.Json;

namespace MovieApp
{
    public class App : Application
    {
        
        public App()
        {   
            Util.tTitles = DatabaseManager.Instance.GetTitles();

            if (Util.tTitles == null)
            {
                Assembly assembly = typeof(App).GetTypeInfo().Assembly;
                Stream stream = assembly.GetManifestResourceStream("MovieApp.Assets.strings.json");
                string text = "";
                using (var reader = new System.IO.StreamReader(stream))
                {
                    text = reader.ReadToEnd();
                }

                var result = JsonConvert.DeserializeObject<Titles>(text);

                DatabaseManager.Instance.AddTitles(result);

                Util.tTitles = DatabaseManager.Instance.GetTitles();
            }

            MainPage = new MainPage();
        }

        public static Page GetMainPage()
        {
            return new MovieApp.MainPage();
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}


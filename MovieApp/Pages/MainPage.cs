﻿using System;

using Xamarin.Forms;

namespace MovieApp
{
    public class MainPage : ContentPage
    {
        public MainPage()
        {
            







            var lblWelcome = new Label
            {
                Text = "Welcome!",
                WidthRequest = 150
            };

            var entryText = new Entry
            {
                    Placeholder = "Entry your name:",
                    WidthRequest = 150,
                    HeightRequest = 50
            };

            Button btnLogin = new Button
            {                   
                Text = "Login",            
            };

            btnLogin.Clicked += (sender, e) => 
            {
                lblWelcome.Text = "Clicked the Button!";


                Navigation.PushModalAsync(new TabPage(entryText.Text));
            };


            Content = new StackLayout
            { 
                    Padding = 20,
                Children =
                {
                    lblWelcome,
                    entryText,
                    btnLogin   
                },
                Spacing = 9,
            };
        }
    }
}



﻿using System;

using Xamarin.Forms;

namespace MovieApp
{
    public class MovieDetailPage : ContentPage
    {
        public MovieDetailPage(Movie movie)
        {    
            this.Title = movie.title;

            Image imgPoster = new Image
            {
                WidthRequest = 200,
                HeightRequest = 320,
                Source = new UriImageSource{Uri = Util.GetImageUri(movie.poster_path)},
                HorizontalOptions = LayoutOptions.Center
            };

            Label lblOverviewTitle = new Label
            {
                Text = "OVERVIEW",
                FontSize = 18,
                TextColor = Color.White
            };

            Label lblOverview = new Label
            {
                Text = movie.overview,
                FontSize = 14,
                TextColor = Color.White
            };

            StackLayout stckOverview = new StackLayout
            {
                Spacing = 4,
                Padding = 6,
                Children = {
                    lblOverviewTitle,
                    lblOverview
                },
                    BackgroundColor = Color.FromRgb(0,183,211),
                    VerticalOptions = LayoutOptions.StartAndExpand
            };

            Button btnFavorite = new Button
            {
                    BorderColor = Color.FromRgb(0,183,211),
                    BorderRadius = 2,
                    BorderWidth = 2,
                    Text = "Favorite",
                    BackgroundColor = Color.White,
                    TextColor = Color.FromRgb(0,183,211)
            };

            Label lblVote = new Label
            {
                Text = movie.vote_average.ToString() + " / 10",
                FontSize = 18,
                TextColor = Color.Black,
                XAlign = TextAlignment.Center,
                    YAlign = TextAlignment.Center
            };

            StackLayout stckFavGroup = new StackLayout
            {
                    Orientation = StackOrientation.Horizontal,
                    Spacing = 20,
                    Padding = 2,
                    Children = {
                        lblVote,
                        btnFavorite
                    },
                    HorizontalOptions = LayoutOptions.Center,
                    VerticalOptions = LayoutOptions.Center
            };

            btnFavorite.Clicked += (sender, e) => 
            {
                    DatabaseManager.Instance.AddMovie(movie);

                    /*
                    btnFavorite.BackgroundColor = Color.FromRgb(0,183,211);
                    btnFavorite.TextColor = Color.White;*/
            };

            var root = new StackLayout
            { 
                Spacing = 4,
                Children =
                {
                    imgPoster,
                            stckFavGroup,
                    stckOverview,
                }
            };
            

            Content = new ScrollView
            {
                Content = root
            };
            
        }
    }
}



﻿using System;
using System.Linq;
using Xamarin.Forms;
using System.Collections.Generic;

namespace MovieApp
{
    public class BoxPage : ContentPage
    {
        ListView lvMovies;
        List<Movie> lstMovies = new List<Movie>();

        public BoxPage()
        {
            Title = Util.tTitles.titleBoxPage;

            lvMovies = new ListView
                {
                    VerticalOptions = LayoutOptions.FillAndExpand
                };

            lvMovies.ItemTemplate = new DataTemplate(typeof(MovieListCell));
            lvMovies.RowHeight = 130;


            Device.BeginInvokeOnMainThread(() =>
            {
                lstMovies = DatabaseManager.Instance.GetMovies().ToList();
                lvMovies.ItemsSource = null;

                lvMovies.ItemsSource = lstMovies.Select(x => 
                {
                    return new MovieListCellHolder{
                        ImageSource = new UriImageSource { Uri = Util.GetImageUri(x.poster_path)},
                        Title = x.title,
                        ReleaseDate = x.release_date
                    };
                }).ToArray();
            });

            lvMovies.ItemTapped += (sender, e) => 
            {
                var item = e.Item as MovieListCellHolder;
                var movieCurr = lstMovies.FirstOrDefault(x => x.title == item.Title);
                Navigation.PushModalAsync(new NavigationPage(new MovieDetailPage(movieCurr)));
            };

            Content = new StackLayout
                { 
                    VerticalOptions = LayoutOptions.FillAndExpand,
                    HorizontalOptions = LayoutOptions.Center,
                    Children =
                        {
                            lvMovies
                        }
                    };
        }

        protected override void OnAppearing()
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                lstMovies = DatabaseManager.Instance.GetMovies().ToList();
                lvMovies.ItemsSource = null;

                lvMovies.ItemsSource = lstMovies.Select(x => 
                    {
                        return new MovieListCellHolder{
                            ImageSource = new UriImageSource { Uri = Util.GetImageUri(x.poster_path)},
                            Title = x.title,
                            ReleaseDate = x.release_date
                        };
                    }).ToArray();
            });
            
            base.OnAppearing();
        }
    }
}



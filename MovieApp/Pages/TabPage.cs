﻿using System;

using Xamarin.Forms;

namespace MovieApp
{
    public class TabPage : Xamarin.Forms.TabbedPage
    {
        public TabPage(string userName)
        {
            
            this.Children.Add(new WelcomePage(userName));
            this.Children.Add(new BoxPage());


        }
    }
}



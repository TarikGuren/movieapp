﻿using System;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Net.Http;

namespace MovieApp
{
    public class ApiManager
    {
        private static string API_KEY = "7df25bca9d2d6dc283c66ffd5ae86483";

        private static Uri sBaseAddress = new Uri("http://api.themoviedb.org/3/");

        public ApiManager()
        {
        }

        public static async Task<List<Movie>> GetPopularMovies()
        {
            using (var httpClient = new HttpClient{ BaseAddress = sBaseAddress })
            {

                httpClient.DefaultRequestHeaders.TryAddWithoutValidation("accept", "application/json");

                using(var response = await httpClient.GetAsync("movie/popular?api_key=" + API_KEY))
                {
                    string responseData = await response.Content.ReadAsStringAsync();

                    var result = JsonConvert.DeserializeObject<MovieResponse>(responseData);

                    return result.results;
                }
            }
        }

    }
}


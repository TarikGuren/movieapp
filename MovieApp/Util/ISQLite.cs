﻿using System;
using SQLite.Net;

namespace MovieApp
{
    public interface ISQLite
    {
        SQLiteConnection GetConnection();
    }
}
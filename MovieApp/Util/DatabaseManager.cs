﻿using System;
using System.IO;
using Xamarin.Forms;
using SQLite.Net;
using SQLite.Net;
using System.Linq;

namespace MovieApp
{
    public class DatabaseManager
    {
        private static DatabaseManager instance;

        public static DatabaseManager Instance
        {
            get 
            {
                if (instance == null)
                {
                    instance = new DatabaseManager();
                }

                return instance;
            }
        }

        SQLiteConnection database;

        public DatabaseManager()
        {
            database = DependencyService.Get<ISQLite> ().GetConnection ();
            database.CreateTable<Movie>();
            database.CreateTable<Titles>();
        }

        //Create
        public int AddMovie(Movie movie)
        {
            return movie._id == 0 ? database.Insert(movie): database.Update(movie);
        }

        //Read
        public Movie GetMovie(int id)
        {
            return database.Get<Movie>(id);
        }

        //Read All
        public Movie[] GetMovies()
        {
            return database.Table<Movie>().ToArray();
        }


        public Titles GetTitles()
        {
            return database.Table<Titles>().FirstOrDefault();
        }

        public int AddTitles(Titles t)
        {
            return t._id == 0 ? database.Insert(t): database.Update(t);
        }
    }

}


﻿using System;
using Xamarin.Forms;

namespace MovieApp
{
    public class Util
    {
        public static Titles tTitles;

        public static Uri GetImageUri(string imagePath)
        {
            return new Uri("https://image.tmdb.org/t/p/w185" + imagePath);
        }


    }
}


﻿using System;

using Xamarin.Forms;
using System.Globalization;

namespace MovieApp
{
    public class MovieListCell : ViewCell
    {
        public MovieListCell()
        {
            
            Image imgPoster = new Image
            {
                WidthRequest = 100,
                HeightRequest = 125,
                    Aspect = Aspect.AspectFit
            };
            
            imgPoster.SetBinding(Image.SourceProperty, "ImageSource");

            var lblTitle = new Label
            {                    
                TextColor = Color.Black,
                FontSize = 18,
                XAlign = TextAlignment.Start
            };

            lblTitle.SetBinding(Label.TextProperty, "Title");

            var lblReleaseDateTitle = new Label
            {
                TextColor = Color.Black,
                FontSize = 14,
                Text = "Release Date : "
            };

            var lblReleaseDate = new Label
            {
                TextColor = Color.Black,
                FontSize = 14,
            };

            lblReleaseDate.SetBinding(Label.TextProperty, "ReleaseDate");

            //ReleaseDate group
            var stckRelease = new StackLayout
            {
                Orientation = StackOrientation.Horizontal,
                Spacing = 2,
                Children = {
                    lblReleaseDateTitle,
                    lblReleaseDate
                }
            };

            //Info Group
            var stckInfo = new StackLayout
            {
                Padding = 6,
                Spacing = 10,
                Children = {
                    lblTitle,
                    stckRelease
                }
            };

            //Root group
            var stckRoot = new StackLayout
            {
                Orientation = StackOrientation.Horizontal,
                Spacing = 7,
                    Padding = 6,
                Children = {
                    imgPoster,
                    stckInfo
                }
            };

//            Content = stckRoot;

            this.View = stckRoot;
        }
    }
}



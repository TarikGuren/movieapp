﻿using System;
using Xamarin.Forms;

namespace MovieApp
{
    public class MovieListCellHolder
    {
        public UriImageSource ImageSource { get; set; }

        public string Title { get; set; }

        public string ReleaseDate { get; set; }
    }
}